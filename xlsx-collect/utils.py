import xlrd
import xlwt

def read_xls(xls_path, xls_sheet=0):
    wb = xlrd.open_workbook(xls_path)
    sheet_names = wb.sheet_names()
    ws = wb.sheet_by_index(xls_sheet)

    first_row = []  # The row where we stock the name of the column
    for col in range(ws.ncols):
        first_row.append(str(ws.cell_value(0, col)).lower())

    data = []
    for row in range(1, ws.nrows):
        elm = {}
        for col in range(ws.ncols):
            value = ws.cell_value(row, col)

            try:
                value = float(value)
            except ValueError:
                pass
            elm[first_row[col]] = value
        data.append(elm)
    return data, first_row


def write_sheet(rows, output_path, headers=None, wb = None, sheet_name="Sheet 1"):
    if wb is None:
        wb = xlwt.Workbook()
    ws = wb.add_sheet(sheet_name)

    if headers is not None:
        for i, hd in enumerate(headers):
            ws.write(0, i, hd)

    for row_index, row in enumerate(rows):
        if headers is None:
            for col_index, elm in enumerate(row.values):
                ws.write(1 + row_index, col_index, elm)
        else:
            for col_index, hd in enumerate(headers):
                if hd in row:
                    elm = row[hd]
                    ws.write(1 + row_index, col_index, str(elm))
    wb.save(output_path)
    return output_path