FROM python:3.7.3

ADD requirements.txt .

RUN apt-get update && apt-get install -y myspell-ru python-enchant

RUN pip3 install --upgrade pip && \
    pip3 install --trusted-host pypi.python.org -r requirements.txt && \
    rm -f requirements.txt

ADD . /code/

WORKDIR /code/

ENTRYPOINT [ "python3", "xlsx-collect/collect.py" ]