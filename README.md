# xlsx-collect

1. (optional) Create a virtualenv (installing python packages globally is a bad practive)

```
virtualenv env
source env/bin/activate
```

1. Build docker image: `docker build . -t xslxcollect`
1. Launch the script: `docker run -v $(pwd):/code/ -ti xlsxcollect --xls-path=tests/bin/example.xlsx`

Params:

    --xls-path : path to xlsx
    --xls-sheet : sheet index (default: 0)
    --output-path  : path to directory with results (default:  "./")
     
    --theta : similarity threshold (default: 0.98)
    --fn-tag : column name for tagging (default: "описание")
    --fn-number : column name for numbers (default: "кол-во")
    --fn-sum : column name for sums (default: "сумма")
    --index-maxlength : max length of TD-IDF index (default: 10000)
    
Directory with results:

    results.xlsx : xlsx with 2 sheets ("from" and "to")
    tokens.json : cached tokenized rows
